#ifndef STT_H
#define STT_H

size_t stt_load_file(const char* file, char** pbuf);
char* stt_send_request(const char* token, const char* audio, size_t size);
void stt_process_response(const char* response);
char* stt_result();


#endif /* 避免重复定义 */